require_relative '../spec_helper.rb'
require 'nestful'
require 'date'

describe PositionRecorder do
  before do
    @collar_id = 'test_collar'
    @position_recorder = PositionRecorder.new('https://klst.firebaseio.com')
  end
  after do
    Nestful.delete("https://klst.firebaseio.com/devices/#{@collar_id}.json")
  end
  describe '#record_position' do
    context "coordinates are valid" do
      it "must record the position" do
        date_time = DateTime.now.to_s
        lat = 123.to_s
        lng = 346.to_s
        location = @position_recorder.record_position(@collar_id, date_time, lat, lng)
        today = Date.today
        entry = Nestful.get("https://klst.firebaseio.com/devices/#{@collar_id}/positions/#{today.year.to_s}-#{today.month.to_s}-#{today.day.to_s}/#{location}.json")
        retrieved_location = JSON.parse(entry.body)
        expect(retrieved_location['date_time']).to eq(date_time)
        expect(retrieved_location['lat']).to eq(lat)
        expect(retrieved_location['lng']).to eq(lng)
      end
    end
    context "coordinates are invalid" do
      it "must not record the position" do
        date_time = DateTime.now.to_s
        lat = ""
        lng = ""
        location = @position_recorder.record_position(@collar_id, date_time, lat, lng)
        today = Date.today
        entry = Nestful.get("https://klst.firebaseio.com/devices/#{@collar_id}/positions/#{today.year.to_s}-#{today.month.to_s}-#{today.day.to_s}/#{location}.json")
        expect(entry.body).to eq("null")
      end
    end    
  end
  describe '#record_last_position' do
    context "coordinates are valid" do
      it "must record the last position" do
        date_time = DateTime.now.to_s
        lat = 123.to_s
        lng = 346.to_s
        location = @position_recorder.record_last_position(@collar_id, date_time, lat, lng)
        entry = Nestful.get("https://klst.firebaseio.com/devices/#{@collar_id}/last-position.json")
        retrieved_location = JSON.parse(entry.body)
        expect(retrieved_location['date_time']).to eq(date_time)
        expect(retrieved_location['lat']).to eq(lat)
        expect(retrieved_location['lng']).to eq(lng)
      end
    end
    context "coordinates are invalid" do
      it "must not record the last position" do
        date_time = DateTime.now.to_s
        lat = ""
        lng = ""
        location = @position_recorder.record_last_position(@collar_id, date_time, lat, lng)
        entry = Nestful.get("https://klst.firebaseio.com/devices/#{@collar_id}/last-position.json")
        expect(entry.body).to eq("null")
      end
    end
  end  
end