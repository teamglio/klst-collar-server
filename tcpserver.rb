require 'eventmachine'
require_relative 'lib/klst_collar_server.rb'

 class KLSTCollarServer < EventMachine::Connection
   def post_init
     puts "Collar connected."
     send_data "Ready\n"
   end

   def receive_data data
     send_data "ACK\n"
     record_data(data)
     close_connection_after_writing
   end

   def unbind
     puts "Collar disconnected."
  end
end

def record_data(data)

  begin
    @position_recorder = PositionRecorder.new('https://klst.firebaseio.com')
    parsed_data = parse_data(data)
    unless parsed_data[:lat].empty? || parsed_data[:lat] == "" || parsed_data[:lng].empty? || parsed_data[:lng] == ""
      @position_recorder.record_position(parsed_data[:device_id], parsed_data[:date_time], parsed_data[:lat], parsed_data[:lng])
      puts "Position recorded"
      @position_recorder.record_last_position(parsed_data[:device_id], parsed_data[:date_time], parsed_data[:lat], parsed_data[:lng])
      puts "Last position recorded"
    else
      puts "Empty coordinates, no position recorded"      
    end
  rescue => error
    puts error
  end
end

def parse_data(data)
  split_data = data.split(',')
  {
    :device_id => split_data[0],
    :date_time => split_data[1],
    :lat => split_data[2],
    :lng => split_data[3]
  }
end

EventMachine.run {
  puts "Started KLST Collar Server..."
  EventMachine.start_server "0.0.0.0", ENV['RUPPELLS_SOCKETS_LOCAL_PORT'] || 8080, KLSTCollarServer
}