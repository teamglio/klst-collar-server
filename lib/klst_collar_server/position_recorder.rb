require 'nestful'
require 'date'

class PositionRecorder
  def initialize(firebase_uri)
    @firebase_uri = firebase_uri
  end
  def record_position(device_id, date_time, lat, lng)
    unless lat == "" || lat.empty? || lng == "" || lng.empty?
      today = Date.today
      url = @firebase_uri + "/devices/#{device_id}/positions/#{today.year.to_s}-#{today.month.to_s}-#{today.day.to_s}.json"
      response = Nestful.post(url, {:device_id => device_id.strip, :date_time => date_time.strip, :lat => lat.strip, :lng => lng.strip}, :format => :json)
      JSON.parse(response.body)['name']
    end
  end
  def record_last_position(device_id, date_time, lat, lng)
    unless lat == "" || lat.empty? || lng == "" || lng.empty?
      url = @firebase_uri + "/devices/#{device_id}/last-position.json"
      response = Nestful.put(url, {:device_id => device_id.strip, :date_time => date_time.strip, :lat => lat.strip, :lng => lng.strip}, :format => :json)
      JSON.parse(response.body)['name']
    end
  end  
end